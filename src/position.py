#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy, time, sys, math, control_lib, tf
import numpy as np
from geometry_msgs.msg import Pose2D, Twist
from turtlesim.msg import Pose
from std_msgs.msg import Bool, Int32
from nav_msgs.msg import Odometry
from sensor_msgs.msg import CameraInfo
import image_geometry

def callback_camera_info(msg):

    global model
    global camera_matrix

    model.fromCameraInfo(msg)
    
    K = np.array(msg.K).reshape([3, 3])
    f = K[0][0]
    u0 = K[0][2]
    v0 = K[1][2]

    camera_matrix[0] = f
    camera_matrix[1] = u0
    camera_matrix[2] = v0

def callback_img_point(msg):
    """
    This function receives the goal and saves it 
    in a globa variable goal
    """

    global camera_height
    global image_point
    global mask_is_true

    # recovering point
    u = msg.x
    v = msg.y
    base_point = [u, v]
    mask_is_true = msg.theta
    distance = 0

    try:
        # finding distance to the point 
        pixel_rectified = model.rectifyPoint(base_point)
        line = model.projectPixelTo3dRay(pixel_rectified)
        th = math.atan2(line[2],line[1])
        distance = math.tan(th) * camera_height

        image_point.x = u
        image_point.y = v
        image_point.theta = distance

    except:
        pass
	

def control_robot():
	
	rospy.Subscriber('/camera/img_base',Pose2D, callback_img_point)   # receives the goal coordinates
	rospy.Subscriber('/camera/rgb/camera_info',CameraInfo, callback_camera_info)   # receives the goal

############ MAIN CODE #######################
camera_height = float(sys.argv[8])
image_point = Pose2D()
mask_is_true = False

camera_matrix = np.zeros((3,1))
model = image_geometry.PinholeCameraModel()

if __name__ == '__main__':
	main()
