#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy, time, sys, math
import numpy as numpy
from geometry_msgs.msg import Pose2D, PoseWithCovarianceStamped, Quaternion, PoseStamped
from actionlib_msgs.msg import GoalID
from tf.transformations import quaternion_from_euler
from move_base_msgs.msg import MoveBaseActionResult
from nav_msgs.msg import Odometry
from time import sleep
from datetime import datetime

###### POSIÇÃO INICIAL ######

def set_init():
	global pwcs_init
	global home_x
	global home_y

	theta = 0.0
	pwcs_init.header.frame_id = 'map'
	pwcs_init.pose.pose.position.x = home_x
	pwcs_init.pose.pose.position.y = home_y
	quat = quaternion_from_euler(0, 0, theta)
	pwcs_init.pose.pose.orientation.x = quat[0]
	pwcs_init.pose.pose.orientation.y = quat[1]
	pwcs_init.pose.pose.orientation.z = quat[2]
	pwcs_init.pose.pose.orientation.w = quat[3]


def ajusta_init():
	pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=10)
	rate = rospy.Rate(10)
	
	if not rospy.is_shutdown():
		rospy.loginfo("X: " + str(pwcs_init.pose.pose.position.x) + " Y: " + str(pwcs_init.pose.pose.position.y))
		pub.publish(pwcs_init)
		sleep(2)
		pub.publish(pwcs_init)
		
		

###### BUSCA ######

def listener_status(data):
	global status
	status = data.status.status
	
def listener_image(data):
	global image_base_x
	global image_base_y
	image_base_x = data.x
	image_base_y = data.y

def set_goals_to_search():
	x = [1.5, 1.5, -1.5]
	y = [-0.5, 0.5, 0.5]
	theta = [1.57, 3.14, -1.57]

	goals = []

	for i in range(3):
		goals.append(PoseStamped())
		goals[i].header.frame_id = 'map'
		goals[i].pose.position.x = x[i]
		goals[i].pose.position.y = y[i]
		quat = quaternion_from_euler(0, 0, theta[i])
		goals[i].pose.orientation.x = quat[0]
		goals[i].pose.orientation.y = quat[1]
		goals[i].pose.orientation.z = quat[2]
		goals[i].pose.orientation.w = quat[3]
		
	return goals
	

def set_goal(x, y):

	goal = PoseStamped()
	goal.header.frame_id = 'map'
	goal.pose.position.x = x
	goal.pose.position.y = y
	quat = quaternion_from_euler(0, 0, 0.0)
	goal.pose.orientation.x = quat[0]
	goal.pose.orientation.y = quat[1]
	goal.pose.orientation.z = quat[2]
	goal.pose.orientation.w = quat[3]
		
	return goal


def go_to_goal(goal):
	pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
	rate = rospy.Rate(3)

	if not rospy.is_shutdown():
		rospy.loginfo(goal)
		pub.publish(goal)
		rate.sleep()
		pub.publish(goal)
		rate.sleep()

def search():
	global pet_status
	goals = set_goals_to_search()
	
	i = 0
	while i < 3:
		go_to_goal(goals[i])
		sleep(2)
		
		while not rospy.is_shutdown():
			rospy.Subscriber('/move_base/result', MoveBaseActionResult, listener_status)
			if status == 3:
				break
			
			rospy.Subscriber('/camera/img_base', Pose2D, listener_image)
			if image_base_x != 0.0 or image_base_y != 0.0:
				i = 5
				pet_status = 3
				break
		if i == 2:
			pet_status = 2
		i += 1




###### MAIN ######
is_set_init = int(sys.argv[1])

# variáveis globais
pwcs_init = PoseWithCovarianceStamped()
status = 0
home_x = -2.0
home_y = -0.5
pet_status = 1
image_base_x = 0.0
image_base_y = 0.0

if __name__ == '__main__':

	rospy.init_node('main')
	home = PoseStamped()
	home = set_goal(home_x, home_y)
	
	# define posição inicial de acordo com o parâmetro
	if is_set_init == 1:
		set_init()
		ajusta_init()
	
	while not rospy.is_shutdown():
		if pet_status == 1: #status1 = fazer a busca
			search()
		elif pet_status == 2: #status2 = ir para a casinha
			go_to_goal(home)
			sleep(2)
			pet_status == 1
		elif pet_status == 3: #status3 = ir até dono
			if image_base_x == 0.0 or image_base_y == 0.0: #dono perdido
				pet_status = 2
			else: #atualizar posições do dono
				rospy.Subscriber('/camera/img_base', Pose2D, listener_image)
			

