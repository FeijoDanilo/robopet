#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy, time, sys, math
import numpy as numpy
from geometry_msgs.msg import Pose2D, PoseWithCovarianceStamped, Quaternion, PoseStamped
from actionlib_msgs.msg import GoalID
from tf.transformations import quaternion_from_euler
from move_base_msgs.msg import MoveBaseActionResult
from nav_msgs.msg import Odometry
from time import sleep
from datetime import datetime
"""
def listener_pos(data):
	rospy.loginfo(data)	
	global pos
    	pos = data.status.status
"""

###### RETORNA POSIÇÃO ATUAL ######

def listener_pos(msg):
	global robot_odom
	robot_odom = msg
	
def odom_robo():
	rospy.Subscriber("odom", Odometry, listener_pos)


###### POSIÇÃO INICIAL ######
"""
def listener_pwcs(msg):
	global pwcs_atual
	pwcs_atual = msg
	
def verifica_init():
	rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, listener_pwcs)
	sleep(2)
"""

def set_init():
	global init
	x = -2.0
	y = -0.5
	theta = 0.0
	pwcs_init.header.frame_id = 'map'
	pwcs_init.pose.pose.position.x = x
	pwcs_init.pose.pose.position.y = y
	quat = quaternion_from_euler(0, 0, theta)
	pwcs_init.pose.pose.orientation.x = quat[0]
	pwcs_init.pose.pose.orientation.y = quat[1]
	pwcs_init.pose.pose.orientation.z = quat[2]
	pwcs_init.pose.pose.orientation.w = quat[3]


def ajusta_init():
	pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=10)
	rate = rospy.Rate(10)
	
	if not rospy.is_shutdown():
		rospy.loginfo("X: " + str(pwcs_init.pose.pose.position.x) + " Y: " + str(pwcs_init.pose.pose.position.y))
		pub.publish(pwcs_init)
		sleep(2)
		pub.publish(pwcs_init)
		
		

###### BUSCA ######

def listener_status(data):
    global status
    status = data.status.status


def set_goals():
	x = [1.5, 1.5, -1.5, -2.0]
	y = [-0.5, 0.5, 0.5, -0.5]
	theta = [1.57, 3.14, -1.57, 0.0]

	goals = []

	for i in range(4):
		goals.append(PoseStamped())
		goals[i].header.frame_id = 'map'
		goals[i].pose.position.x = x[i]
		goals[i].pose.position.y = y[i]
		quat = quaternion_from_euler(0, 0, theta[i])
		goals[i].pose.orientation.x = quat[0]
		goals[i].pose.orientation.y = quat[1]
		goals[i].pose.orientation.z = quat[2]
		goals[i].pose.orientation.w = quat[3]
		
	return goals


def go_to_goal(goal):
	pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
	rate = rospy.Rate(3)

	if not rospy.is_shutdown():
		rospy.loginfo(goal)
		pub.publish(goal)
		rate.sleep()
		pub.publish(goal)
		print("publicou")


def search():
	goals = set_goals()
	
	for i in range(4):
		go_to_goal(goals[i])
		sleep(2)
		
		while not rospy.is_shutdown():
			rospy.Subscriber('/move_base/result', MoveBaseActionResult, listener_status)
			if status == 3:
				break


###### GO_TO_MASTER ######

def callback_robot_goal(msg):
	global goal
	goal = msg

def calc_error(goal):
	global robot_odom
	odom_robo()

	# recuperando as coordenadas do robo
	x = robot_odom.pose.pose.position.x
	y = robot_odom.pose.pose.position.y

	# recuperand o goal
	x_d = goal.pose.position.x
	y_d = goal.pose.position.y

	# definindo os erros
	delta_x = x_d - x
	delta_y = y_d - y

	erro_p = round(math.sqrt(delta_x**2 + delta_y**2),3)

	return erro_p

def go_to_master():
	global master
	testes = []

	testes = set_goals()

	go_to_goal(testes[0])
	sleep(3)
	
	while not rospy.is_shutdown():
		erro = calc_error(testes[0])
		print(erro)
		if erro <= 1.0:
			print("paaaaaaaaaaaaaaaaaaaaaaaaaaaaaaraaaaaaaaaaaaaaaa")
			break

###### MAIN ######
# posições
robot_odom = Odometry()
master = PoseStamped()
pwcs_init = PoseWithCovarianceStamped()
pwcs_atual = PoseWithCovarianceStamped()

# variáveis globais
status = 0
localized = True

if __name__ == '__main__':
	#global goal
	
	rospy.init_node('main')
	
	# define posição inicião se não estiver definido
	#verifica_init()
	set_init()
	#ajusta_init()	
	
	#search()
	go_to_master()
	"""
	if rospy.is_shutdown():
		while status != 3:
			sleep(1)
		go_to_home()"""

###### GO_TO_HOME ######
"""
def set_home():
	x = 2.0
	y = -0.5
	theta = 0.0
	
	goal = PoseStamped()
	goal.header.frame_id = 'map'
	goal.pose.position.x = x
	goal.pose.position.y = y
	
	quat = quaternion_from_euler(0, 0, theta)
	goal.pose.orientation.x = quat[0]
	goal.pose.orientation.y = quat[1]
	goal.pose.orientation.z = quat[2]
	goal.pose.orientation.w = quat[3]
	
	return goal


def go_to_home():
	home = set_home()
	pub = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
	rate = rospy.Rate(4)

	if not rospy.is_shutdown():
		rospy.loginfo(home)
		pub.publish(home)
		rate.sleep()
		pub.publish(home)
"""
